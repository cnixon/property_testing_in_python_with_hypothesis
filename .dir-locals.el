((org-mode . ((eval . (let ((d (car (dir-locals-find-file "."))))
                        (setq-local org-publish-project-alist
                                    `(("presentation"
                                       :base-directory ,(concat d "org")
                                       :base-extension "org"
                                       :publishing-directory ,(concat d "public")
                                       :publishing-function org-reveal-publish-to-reveal
                                       :auto-sitemap nil ; t
                                       ))))))))
